# Kubernetes Cluster

[![pipeline status](https://gitlab.home.mylocal/home-lab/kubernetes-cluster/badges/main/pipeline.svg)](https://gitlab.home.mylocal/home-lab/kubernetes-cluster/-/commits/main) [![coverage report](https://gitlab.home.mylocal/home-lab/kubernetes-cluster/badges/main/coverage.svg)](https://gitlab.home.mylocal/home-lab/kubernetes-cluster/-/commits/main) [![Latest Release](https://gitlab.home.mylocal/home-lab/kubernetes-cluster/-/badges/release.svg)](https://gitlab.home.mylocal/home-lab/kubernetes-cluster/-/releases)

Below is a writeup on how to deploy this repository. This repository is a bare-bones install of a production grade kubernetes cluster. This repository will deploy all virtual machines necessary and install all required kubernetes clusters.

Towards the bottom will be scratch notes that might not make any sense :).

## Dependencies

- Terraform
- Ansible
- Kubespray
- Python 3.9

## Deployment Steps

This deployment relies on Proxmox VE, terraform and ansible. By the end of this deployment you will have a complete production quality Kubernetes cluster.

### Preparation

In order to properly deploy, there are a number of steps for you to complete prior to deployment. First, you should have the following variables in your CI as they will be parsed via `.gitlab-ci.yml` file.:

- PROXMOX_HOST_URL - URL to Proxmox where VM's will be deployed.
- API_TOKEN_ID - From Proxmox
- API_TOKEN_SECRET - From Proxmox
- K8_GATEWAY - Gateway address for the network segment.
- K8_NAMESERVER - DNS server IP address

You must also create a new public/private key in order to facilitate ansible SSH into each VMs.

### Deploy and Install Kubernetes

#### Deploy the Virtual Machines

Once the above variables are set you may run the first commands, this will create the virtual machines required. These commands should be ran from the root of the directory.:

```bash
terraform plan -var "pm_token_secret=${API_TOKEN_SECRET}" -var "k8s_gateway=${K8_GATEWAY}" -var "k8s_nameserver=${K8_NAMESERVER}" -var "pm_host_url=${PROXMOX_HOST_URL}" -var "pm_api_token_id=${API_TOKEN_ID}" -out <output file>
terraform apply <output file>
```

The above command will do nothing more than stand up VMs. In order to change their configurations you may see the `variables.tf` file. Anything configurable will be made accessible in this file.

#### Install & Configure Kubernetes

After the terraform process is complete and you have successfully created the virtual machines, you will need to change directories to the `ansible/kubespray` directory.

Next you will need to install kubernetes. To do this run the following ansible command to install kubernetes and the created VMs:

```bash
ansible-playbook -i inventory/kubecluster/inventory.ini -u ansible --become --become-user=root cluster.yml --private-key=<path-to-ssh-key> --ask-vault-pass
```

## Configurations

This repository is designed to be customizable. The end-user should only need to change a few configuration files in order to change the cluster to their needs.

### VM Configuration

Currently the deployment script is set to configure the VMs accordingly, you may change the values at `variables.tf`:

Master Nodes:

- Count: 1
- CPU: 4
- Memory: 4096 MB
- Disk Size: 300G

Etcd Nodes:

- Count: 1
- CPU: 4
- Memory: 4096 MB
- Disk Size: 300G

Worker Nodes:

- Count: 3
- CPU: 4
- Memory: 8192 MB
- Disk Size: 300G

### Kubernetes Configurations

The kubernetes cluster that is spun up relies on metallb to handle load balancing traffic. This has its drawbacks but one positive is that it requires one less VM in the mix.

In order to make changes to the cluster you will need to update files in the `ansible/kubespray/inventory/kubecluster` directory. Within this directory there are two paths:

1. `group_vars/all/all.yml`

   The file(s) here that are important are `all.yml`. This file handles configuration of external load balancers.

2. `k8s_cluster/*`

   There are a few files here that are used for configuration.

   - `addons.yml` can be used to configure additional functionality such as metallb, cert_manager, and other useful functions.
   - `cluster.yml` handles configurations for the whole kubernetes cluster. Internal networking and other configurations are exposed in this file.

### Connect to Gitlab KAS

```bash
helm upgrade --install dev-agent1 gitlab/gitlab-agent     --namespace gitlab-agent-dev-agent1     --create-namespace     --set image.tag=v16.3.0     --set config.token=     --set config.kasAddress=wss://gitlab.home.mylocal/-/kubernetes-agent/ --set-file config.caCert=/home/t0x/Downloads/internal-ca-new.crt
```

### Installing Kubernetes Dashboard

1. Copy files sa.yml and clusterrole.yml to master-0 node.
2. Run the following commands:

   ```bash

   kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml
   <!-- kubectl create serviceaccount <sa_name> -n default
   kubectl create clusterrolebinding <sa_name>-admin -n default --clusterrole=cluster-admin --serviceaccount=default:<sa_name> -->
   kubectl apply -f dashboard/sa-secret.yml #replace change_me with sa_name in file.
   kubectl apply -f dashboard/clusterrole.yml
   kubcetl create token <sa_name>
   kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/sa-dev -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
   kubectl port-forward svc/kubernetes-dashboard 443:9000
   ```

3. Retreive the token from the final kubectl command and save it to be able to login to the dashboard.
4. create reverse tunnel into the master node:

   ```bash
       ssh -L 8001:127.0.0.1:8001 ubuntu@10.98.1.71
       kubectl proxy # from within the ssh tunnel created.
   ```

5. Dashboard can be found [here](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/persistentvolume?namespace=default)
6. Login with the token retreived above.

## Other Things To Do In K8s

### Connect Gitlab repo to K8s

```bash
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install first-agent gitlab/gitlab-agent     --namespace gitlab-agent-first-agent     --create-namespace     --set image.tag=v16.2.0     --set config.token=qdtT4zfQEZ9LrgupHyxaMLyDXoxxF7b9QhLF_aKnhxJ3pgM3GQ     --set config.kasAddress=wss://gitlab.home.mylocal/-/kubernetes-agent/ --set-file config.caCert=/home/t0x/Downloads/internal-ca-new.crt
```

## Next Steps

1. Configure nginx ingress controller.

## Tutorial to follow

<https://fajlinuxblog.medium.com/kubernetes-deploy-a-production-ready-cluster-with-ansible-ceaf7d3a9997>

<https://stackoverflow.com/questions/39864385/how-to-access-expose-kubernetes-dashboard-service-outside-of-a-cluster>

<https://www.thegeekdiary.com/how-to-access-kubernetes-dashboard-externally/>

Additional Steps:

```bash
kubectl apply -f inventory/kubecluster/sa-secret.yml
```

```bash
ansible-playbook -i inventory.ini haproxy.yml --private-key=../../../ansible-ssh -u ansible
ansible-playbook -i inventory/kubecluster/inventory.ini -u ansible --become --become-user=root cluster.yml --private-key=../../ansible-ssh
```

## Manual Steps

### Commands to be ran

Install Kubernetes dependencies.

```bash
 ansible-playbook -i ansible-hosts.txt ansible-install-kubernetes-dependencies.yml --private-key=<priv_key_location>
```

Initialize HA Proxy node

```bash
ansible-playbook -i inventory.ini haproxy.yml --private-key=../../../ansible-ssh -u ansible
```

Initialize Kubernetes cluser

```bash
 ansible-playbook -i ansible-hosts.txt ansible-init-cluster.yml --private-key=<priv_key_location>
```

Join cluster nodes together.

```bash
 ansible-playbook -i ansible-hosts.txt ansible-get-join-command.yml --private-key=<priv_key_location>
```

```bash
ansible-playbook -i ansible-hosts.txt ansible-join-workers.yml --private-key=<priv_key_location>
```

Install Dashboard on master node:

1. Copy files sa.yml and clusterrole.yml to master-0 node.
2. Run the following commands:

   ```bash

   kubectl apply -f sa.yaml
   kubectl apply -f clusterrole.yaml
   kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/admin-user -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"

   ```

3. Retreive the token from the final kubectl command and save it to be able to login to the dashboard.
4. create reverse tunnel into the master node:

   ```bash
       ssh -L 8001:127.0.0.1:8001 ubuntu@10.98.1.71
       kubectl proxy # from within the ssh tunnel created.
   ```

5. Dashboard can be found [here](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/persistentvolume?namespace=default)
6. Login with the token retreived above.
