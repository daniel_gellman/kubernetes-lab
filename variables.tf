variable "ssh_key" {
  description = "Allowed public keys.  Should only be my laptop and desktop for now."
  type        = list(string)
  # default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDcwZAOfqf6E6p8IkrurF2vR3NccPbMlXFPaFe2+Eh/8QnQCJVTL6PKduXjXynuLziC9cubXIDzQA+4OpFYUV2u0fAkXLOXRIwgEmOrnsGAqJTqIsMC3XwGRhR9M84c4XPAX5sYpOsvZX/qwFE95GAdExCUkS3H39rpmSCnZG9AY4nPsVRlIIDP+/6YSy9KWp2YVYe5bDaMKRtwKSq3EOUhl3Mm8Ykzd35Z0Cysgm2hR2poN+EB7GD67fyi+6ohpdJHVhinHi7cQI4DUp+37nVZG4ofYFL9yRdULlHcFa9MocESvFVlVW0FCvwFKXDty6askpg9yf4FnM0OSbhgqXzD austin@EARTH"
  default = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDYvj3Li3A7tKCAdwHQCm35+g1BS5nHxa81WdAe+MwIt4n8aQBRlQWp1gsZ37TpJCVL0VhAopLrCMtZYgk8YMTnd6IUjwAz7z24USoAf6i4eWLinqxhkl+XjQO00bgoyXnLhVrxwvUqu2Mgw8YXfJzbjK4fsUfAVCmUPG9lPunAlJSKl6siFqPxQ/guj7X0r/2xVroxqsJ+sEj34fNop8xPLAmKuQ1MoanW7L/SAiNKw3QnjAQl0rZUH4ZXGbsQxXGWEqvQLntujjOnbM9m2AGNVZzrb2jMsbiL0QNFxi0xK+TuVZrW0GOi9ecpewhAat0nWr6kVJVNdrzzQ5EQi0p3UDLEXsWLiIhIy4w25kTUb80scCZTJ/3B3ole/I3ZbloQ4EwHY+kfHJP+5zge09rsIE9WwfPF38ApitnGiyD3CtNLe6D2TgGT3TysMCpQhbPJHEF9xzAMmaaVD3LZ/0D3QdsSXvloA4HJwYdmXtyW8jOTII7mMEEWauY3KTgJfHk= t0x@t0x",
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDB7ZBzrsX1kSGcUXs5Ux1Joq8aTheuClvRzewyq456EgSRTdqEVpQNK843k3fs98aTwRs0pdj3PkmM3UPgMTqEa2bQU1Ay58ixL+7TmX4JrqeJM4flfFmKKUfVoMB7RNCCXRTKwG7scQMJvqk7i8iucIeWJZFDuB1h4o/feDNw14BoEfaguwv1ShnWcnaccTvsSudE92fE9CNdPa1kj3C3GH3gmEAEAZd6HkwBUBQBahpA1tQWTwLUFq//V1NHfRDM/9AZNPOMT2jQm+oNfMkmlnN4GGczk1Nl3tXyOUqsYW+Prpmn2DF4FSV1gfIDRdC6cssywqK25xvscURoCTE5VuSmhcSb2aYMaF2OkqEHwHjmayY6EEv//z6uMgVCgSqeWAaHdXmOuM+tM9iBl9k4ED9iERZpH2D/Nfq1t0+0VozQeB1MdL+CroPeeY8RAcGk04GPKjWkwTNxhYt9a7MkMQkrDa6rcZ8dGG8f+R2x37892BCwjXIRyWUGpKEg2AE= t0x@t0x",
    "ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBAHKtcoGBVJHL/n+GY4VwvGmuxS+i4EJTfOnUX970LVa28qoltpBXXiRW1aiimfg+R/mstSzkb6ROrnylP5CQHlWlgDA4HVu5hHEKYoaO8kH+MdTPG+b0TOY916OW2PfOFbhEbRUwbkuKw+XQAMhioczwjzND0PqMUvk3fw8Lfqs04yNeg== dgellman3@MacBook-Pro-3.home.mylocal"
  ]
}
variable "user" {
  default     = "ansible"
  description = "username to login with"
  type        = string
}


variable "template_name" {
  default = "ubuntu-2004-cloudinit-template"
}


variable "pm_host_url" {
  default     = "https://pve.home.mylocal:8006/api2/json"
  description = "host url for proxmox server"
  type        = string
}

variable "target_node" {
  default = "pve"
}

variable "pm_token_secret" {
  default     = "token_here"
  description = "api token secret for proxmox user"
  type        = string
}

variable "pm_api_token_id" {
  default     = "token_here"
  description = "api token id for proxmox user"
  type        = string
}


## VM specific variables
variable "num_k8s_masters" {
  default = 3
}


variable "k8s_master_vm_id" {
  description = "Sets the VM IDs for each master virtual machine"
  default     = 220
}

variable "k8s_master_vm_name" {
  description = "Sets the VM prefix name for each master virtual machine"
  default     = "k8s-dev-master"
}

variable "k8s_master_ip_addresses" {
  description = "List of IP addresses for master node(s)"
  type        = list(string)
  default     = ["10.20.2.104", "10.20.2.105", "10.20.2.106"]
}

variable "num_k8s_master_mem" {
  default = "8192"
}

variable "k8s_master_cores" {
  default = "4"
}

variable "k8s_master_root_disk_size" {
  default = "32G"
}

variable "k8s_master_disk_storage" {
  default = "local-lvm"
}

variable "k8s_etcd_vm_id" {
  description = "Sets the VM IDs for each master virtual machine"
  default     = 230
}

variable "k8s_etcd_vm_name" {
  description = "Sets the VM prefix name for each master virtual machine"
  default     = "k8s-dev-etcd"
}

variable "num_k8s_etcds" {
  default = 3
}

variable "k8s_etcd_ip_addresses" {
  description = "List of IP addresses for etcd node(s)"
  type        = list(string)
  default     = ["10.20.2.107", "10.20.2.108", "10.20.2.109"]
}

variable "num_k8s_etcd_mem" {
  default = "4096"
}

variable "k8s_etcd_cores" {
  default = "4"
}

variable "k8s_etcd_root_disk_size" {
  default = "32G"
}
variable "k8s_etcd_disk_storage" {
  default = "local-lvm"
}

variable "k8s_worker_vm_id" {
  description = "Sets the VM IDs for each master virtual machine"
  default     = 240
}

variable "k8s_worker_vm_name" {
  description = "Sets the VM prefix name for each master virtual machine"
  default     = "k8s-dev-worker"
}

variable "num_k8s_workers" {
  default = 5
}

variable "k8s_worker_ip_addresses" {
  description = "List of IP addresses for worker node(s)"
  type        = list(string)
  default     = ["10.20.2.110", "10.20.2.111", "10.20.2.112", "10.20.2.113", "10.20.2.114", "10.20.2.115", "10.20.2.116", "10.20.2.117"]
}

variable "num_k8s_node_mem" {
  default = "28672"
}

variable "k8s_node_cores" {
  default = "8"
}

variable "k8s_node_root_disk_size" {
  default = "2000G"
}

variable "k8s_node_disk_storage" {
  default = "local-lvm"
}

variable "num_k8s_elbs" {
  default = 0
}

variable "k8s_elb_ip_addresses" {
  description = "List of IP addresses for elb node(s)"
  type        = list(string)
  default     = ["10.20.2.113"]
}

variable "num_k8s_elb_mem" {
  default = "4096"
}

variable "k8s_elb_cores" {
  default = "4"
}

variable "k8s_elb_root_disk_size" {
  default = "32G"
}

variable "k8s_elb_data_disk_size" {
  default = "250G"
}

variable "k8s_elb_disk_storage" {
  default = "local-lvm"
}

variable "k8s_gateway" {
  type    = string
  default = "10.20.1.1"
}

variable "k8s_vlan" {
  default = 33
}

variable "template_vm_name" {
  default = "k8s-template"
}

variable "k8s_nameserver_domain" {
  type    = string
  default = "domain.ld"
}

variable "k8s_nameserver" {
  type    = string
  default = "10.10.1.3"
}

variable "k8s_user" {
  default = "ansible"
}

variable "pve_host" {
  default   = "pve.home.mylocal"
  type      = string
  sensitive = true
}
