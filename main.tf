terraform {
  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "2.9.14"
    }
  }
  backend "http" {
  }
}

provider "proxmox" {
  pm_api_url          = var.pm_host_url # change this to match your own proxmox
  pm_api_token_id     = var.pm_api_token_id
  pm_api_token_secret = var.pm_token_secret
  pm_tls_insecure     = true
  pm_log_enable       = true
  pm_debug            = true
  pm_log_file         = "terraform-plugin-proxmox.log"
  pm_timeout          = 600
  pm_parallel         = 2
  pm_log_levels = {
    _default    = "debug"
    _capturelog = ""
  }
}


resource "proxmox_vm_qemu" "k8s-master" {

  count       = var.num_k8s_masters
  name        = "${var.k8s_master_vm_name}-${count.index}"
  desc        = "k8s Master Node"
  ipconfig0   = "gw=${var.k8s_gateway},ip=${var.k8s_master_ip_addresses[count.index]}/22"
  target_node = var.target_node
  tags        = "ubuntu,${var.k8s_master_vm_name},terraform,dev"
  vmid        = var.k8s_master_vm_id + count.index + 1 # should start at 1


  # Same CPU as the Physical host, possible to add cpu flags
  # Ex: "host,flags=+md-clear;+pcid;+spec-ctrl;+ssbd;+pdpe1gb"
  cpu        = "host"
  sockets    = 1
  numa       = false
  clone      = var.template_name
  os_type    = "cloud-init"
  agent      = 1
  ciuser     = var.k8s_user
  memory     = var.num_k8s_master_mem
  cores      = var.k8s_master_cores
  nameserver = var.k8s_nameserver


  sshkeys = <<EOT
    %{for key in var.ssh_key}
      ${key}
    %{endfor}
  EOT

  # scsihw     = "virtio-scsi-pci"
  scsihw   = "virtio-scsi-single"
  bootdisk = "scsi0"

  network {
    model  = "virtio"
    bridge = "vmbr1"
    # tag    = var.k8s_vlan
  }

  serial {
    id   = 0
    type = "socket"
  }

  vga {
    type = "serial0"
  }

  disk {
    slot     = 0
    size     = var.k8s_master_root_disk_size
    storage  = var.k8s_master_disk_storage
    type     = "scsi"
    iothread = 1
    backup   = true
  }


  lifecycle {
    ignore_changes = [
      network,
    ]
  }
}

resource "proxmox_vm_qemu" "k8s-etcd" {

  count       = var.num_k8s_etcds
  name        = "${var.k8s_etcd_vm_name}-${count.index}"
  desc        = "k8s etcd Node"
  ipconfig0   = "gw=${var.k8s_gateway},ip=${var.k8s_etcd_ip_addresses[count.index]}/22"
  target_node = var.target_node
  tags        = "ubuntu;${var.k8s_etcd_vm_name};terraform;dev"
  vmid        = var.k8s_etcd_vm_id + count.index + 1 # should start at 1

  # Same CPU as the Physical host, possible to add cpu flags
  # Ex: "host,flags=+md-clear;+pcid;+spec-ctrl;+ssbd;+pdpe1gb"
  cpu        = "host"
  sockets    = 1
  numa       = false
  clone      = var.template_name
  os_type    = "cloud-init"
  agent      = 1
  ciuser     = var.k8s_user
  memory     = var.num_k8s_etcd_mem
  cores      = var.k8s_etcd_cores
  nameserver = var.k8s_nameserver


  sshkeys = <<EOT
    %{for key in var.ssh_key}
      ${key}
    %{endfor}
  EOT

  # scsihw     = "virtio-scsi-pci"
  scsihw   = "virtio-scsi-single"
  bootdisk = "scsi0"

  network {
    model  = "virtio"
    bridge = "vmbr1"
    # tag    = var.k8s_vlan
  }

  serial {
    id   = 0
    type = "socket"
  }

  vga {
    type = "serial0"
  }

  disk {
    slot     = 0
    size     = var.k8s_etcd_root_disk_size
    storage  = var.k8s_etcd_disk_storage
    type     = "scsi"
    iothread = 1
    backup   = true
  }


  lifecycle {
    ignore_changes = [
      network,
    ]
  }
}


resource "proxmox_vm_qemu" "k8s-worker" {

  count       = var.num_k8s_workers
  name        = "${var.k8s_worker_vm_name}-${count.index}"
  desc        = "k8s Worker Node"
  ipconfig0   = "gw=${var.k8s_gateway},ip=${var.k8s_worker_ip_addresses[count.index]}/22"
  target_node = var.target_node
  tags        = "ubuntu,${var.k8s_worker_vm_name},terraform,dev"
  vmid        = var.k8s_worker_vm_id + count.index + 1 # should start at 1

  # Same CPU as the Physical host, possible to add cpu flags
  # Ex: "host,flags=+md-clear;+pcid;+spec-ctrl;+ssbd;+pdpe1gb"
  cpu        = "host"
  numa       = false
  clone      = var.template_name
  os_type    = "cloud-init"
  agent      = 1
  ciuser     = var.k8s_user
  memory     = var.num_k8s_node_mem
  cores      = var.k8s_node_cores
  nameserver = var.k8s_nameserver
  scsihw     = "virtio-scsi-single"
  bootdisk   = "scsi0"



  sshkeys = <<EOT
    %{for key in var.ssh_key}
      ${key}
    %{endfor}
  EOT

  network {
    model  = "virtio"
    bridge = "vmbr1"
    # tag    = var.k8s_vlan
  }

  serial {
    id   = 0
    type = "socket"
  }

  vga {
    type = "serial0"
  }

  disk {
    slot     = 0
    size     = var.k8s_node_root_disk_size
    storage  = var.k8s_node_disk_storage
    type     = "scsi"
    backup   = true
    iothread = 1
  }

  lifecycle {
    ignore_changes = [
      network, target_node
    ]
  }
}
