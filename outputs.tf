output "Master-IPS" {
  value = ["${proxmox_vm_qemu.k8s-master.*.default_ipv4_address}"]
}
output "worker-IPS" {
  value = ["${proxmox_vm_qemu.k8s-worker.*.default_ipv4_address}"]
}
