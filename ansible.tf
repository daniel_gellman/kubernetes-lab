# resource "null_resource" "run_ansible" {

#   provisioner "local-exec" {
#     command     = "ansible-playbook -i ansible/ansible-hosts.txt ansible/ansible-install-kubernetes-dependencies.yml"
#     working_dir = "ansible"
#     environment = {
#       ANSIBLE_HOST_KEY_CHECKING = "false"
#     }
#   }
# }

resource "local_file" "ansible_inventory" {
  filename = "./ansible/kubecluster/inventory.ini"
  content  = <<-EOF

  [all]
  # Add each master node via ansible
  %{for instance in proxmox_vm_qemu.k8s-master~}
  ${instance.name} ansible_host=${trimsuffix(substr(instance.ipconfig0, 16, length(instance.ipconfig0) - 3), "/22")} ip=${trimsuffix(substr(instance.ipconfig0, 16, length(instance.ipconfig0) - 3), "/22")}
  %{endfor}
  # Add each worker node via ansible
  %{for instance in proxmox_vm_qemu.k8s-worker~}
  ${instance.name} ansible_host=${trimsuffix(substr(instance.ipconfig0, 16, length(instance.ipconfig0) - 3), "/22")} ip=${trimsuffix(substr(instance.ipconfig0, 16, length(instance.ipconfig0) - 3), "/22")}
  %{endfor}
  # Add each etcd node via ansible
  %{for instance in proxmox_vm_qemu.k8s-etcd~}
  ${instance.name} ansible_host=${trimsuffix(substr(instance.ipconfig0, 16, length(instance.ipconfig0) - 3), "/22")} ip=${trimsuffix(substr(instance.ipconfig0, 16, length(instance.ipconfig0) - 3), "/22")}
  %{endfor}

  [kube_control_plane]
  %{for instance in proxmox_vm_qemu.k8s-master~}
  ${instance.name}
  %{endfor}


  [etcd]
  %{for instance in proxmox_vm_qemu.k8s-etcd~}
  ${instance.name}
  %{endfor}


  [kube_node]
  %{for instance in proxmox_vm_qemu.k8s-worker~}
  ${instance.name}
  %{endfor}

  [calico_rr]

  [k8s_cluster:children]
    kube_control_plane
    kube_node
    cilium_rr
  EOF

  # provisioner "local-exec" {
  #   command     = "ansible-playbook -i kubespray/inventory/kubecluster/inventory.ini -u ansible --become --become-user=root kubespray/cluster.yml --private-key=ansible-ssh"
  #   working_dir = "ansible"
  #   environment = {
  #     ANSIBLE_HOST_KEY_CHECKING = "false"
  #   }
  # }
}

locals {

}
